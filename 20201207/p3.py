#Inheritance and Polymorphism
"""
Animal is a structure consisting of
    Neyes AS int
    Nmouth AS int
    Nnose AS int
    Nears AS int
"""
class Animal:
    def __init__(self,Neyes,Nmouth,Nnose,Nears):
        self.Neyes = Neyes
        self.Nmouth = Nmouth
        self.Nnose = Nnose
        self.Nears = Nears
    def sounding(self):
        print("aewwwwwww")

class Dog(Animal):
    def __init__(self):
        Animal.__init__(self,2,1,1,2)
    def sounding(self):
        print("Hong Hong")
        
Dang = Dog()
#Encapsulation
class person:
    def __init__(self,name,age,sex,blood):
        self.name = name #Public
        self.age = age
        self.sex = sex
        self.__blood = blood #Private
        self.__Nchromosome = 23
    def changeB(self,newB):
        self.__blood = newB
    def showB(self):
        print(self.__blood)
        
    def __str__(self):
        return "NAME: {}, AGE: {}, Blood: {}".format(self.name,self.age,self.__blood)

        
Pom=person("Pravit",60,"men","A")
Pom.changeB("O")
Pom.showB()
print(Pom)
