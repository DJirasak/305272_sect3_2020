"""
OOP : Object-oiented Programming
    1. abstraction
        => การแสดง Attributes และ Method ของ Object เท่าที่จำเป็น
    2. Inheritance
        => เป็นหลักการที่ คลาสแม่ ( Super Class ) สามารถสืบทอด 
        Attributes และ Method ต่างๆ ไปยัง คลาสลูก ( Sub Class ) ได้ 
    3. Polymorphism
        => วัตถุใหม่ที่เกิดจากวัตถุแม่ชนิดเดียวกัน 
        มีความสามารถเหมือนแม่ แต่การผลลัพธ์การทำงานไม่เหมือนกัน โดยมีลักษณะเฉพาะตัว
    4. Encapsulation
        => วิธีการในการซ่อนข้อมูลหรือจำกัดการเข้าถึงข้อมูลบางอย่าง 
        โดยหากต้องการเข้าถึงข้อมูลให้เข้าถึงผ่านทาง Methods 
        หรือเราสามารถกำหนดระดับการเข้าถึงได้ ยกตัวอย่างเช่น public , private

Person
    -attributes/Properties
        -name
        -BornDate
        -hair color
        -height
        -weight
        -sex
        -age
        -national
    +method/Behavior
        +sleep()
        +run()
        +speak()
        +eat()
        +walk()
        +breathe()
        
Car
    -attributes/Properties
        -สี
        -รุ่น
        -ปี
    +method/Behavior
        +ขับ()
        +เบรก()
        +เปิดไฟเลี้ยว()
        +บีบแตร()

    bike() #parent class
        -ล้อ
        -แฮนด์
        -เบรก
        -กระดิ่ง
        +ride()
        +break()
        +ringing()

    motorbike(bike) #child class
        -ล้อ
        -แฮนด์
        -เบรก
        -แตร
        +ride()
        +break()
        +ringing()

    Animal()
        -eyes
        -mouth
        -nose
        -ears
        +barking
        
        
    Dog(animal)
        -eyes
        -mouth
        -nose
        -ears
        +barking ==>> Hong Hong
        
    Cat(animal)
        -eyes = red
        -mouth
        -nose
        -ears
        +barking ==>> Meaw Meaw 
        
    DCat(Cat)
        Cat(animal)
        -eyes = red
        -mouth
        -nose
        -ears
        +barking ==>> Ngaow Ngaow
        
    
    

    
"""

